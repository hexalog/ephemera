## Installation

```bash
pip3 install -r pip_requirements.txt
```

## Running

```bash
python3 manage.py runserver
```

## Using
open 'http://127.0.0.1:8000/demo/' by browser
