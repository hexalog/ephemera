from django.core.exceptions import PermissionDenied
from django.http import Http404
from django.utils import six
from django.utils.translation import ugettext_lazy as _
from rest_framework import status, exceptions
from rest_framework.response import Response
import sys
import logging

logger = logging.getLogger(__name__)

def exception_handler(exc, context):

    logger.exception(msg=sys.exc_info())

    if hasattr(exc, 'http_status_code'):
        headers = {}
        data = {'non_field_errors': [six.text_type(exc)]}
        if hasattr(exc, 'properties'):
            data.update(exc.properties())
        return Response(data, status=exc.http_status_code, headers=headers)

    if isinstance(exc, exceptions.ValidationError):
        headers = {}
        data = {'validation': exc.detail}
        return Response(data, status=exc.status_code, headers=headers)
    elif isinstance(exc, exceptions.APIException):
        headers = {}
        if getattr(exc, 'auth_header', None):
            headers['WWW-Authenticate'] = exc.auth_header
        if getattr(exc, 'wait', None):
            headers['Retry-After'] = '%d' % exc.wait
        if isinstance(exc.detail, dict):
            data = exc.detail
        elif isinstance(exc.detail, list):
            data = {'non_field_errors': exc.detail}
        else:
            data = {'non_field_errors': [exc.detail]}
        return Response(data, status=exc.status_code, headers=headers)
    elif isinstance(exc, Http404):
        msg = _('Not found.')
        data = {'non_field_errors': [six.text_type(msg)]}
        return Response(data, status=status.HTTP_404_NOT_FOUND)
    elif isinstance(exc, PermissionDenied):
        msg = _('Permission denied.')
        data = {'non_field_errors': [six.text_type(msg)]}
        return Response(data, status=status.HTTP_403_FORBIDDEN)
    else:
        msg = _('Server Error. Please try again later.')
        data = {'non_field_errors': [six.text_type(msg)]}
        return Response(data, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
