from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.response import Response
from pymongo import MongoClient

#MONGODB_HOST = "localhost"
MONGODB_HOST = "mongo"
MONGODB_PORT = 27017
MONGODB_DATABASE = "faas_demo"
MONGODB_COLLECTION = "car_events"
MONGODB_CONNECTION_URL = "mongodb://" + MONGODB_HOST + ":" + str(MONGODB_PORT) + "/" + MONGODB_DATABASE
# MONGODB_CONNECTION_URL = "mongodb://mongo-0.mongo,mongo-1.mongo,mongo-2.mongo:27017/faas_demo\_?"

class Repository:
    def connect(self):
        try:
            self.mongoclient = MongoClient(MONGODB_CONNECTION_URL)
            self.db = eval('self.mongoclient.' + MONGODB_DATABASE)
            self.collection = eval('self.db.' + MONGODB_COLLECTION)
            return True
        except:
            print('car_function: mongo connection failed {}'.format(sys.exc_info()[0]))
            return False

    def get_car_positions(self):
        cursors = self.collection.find()
        positions = []
        for cursor in cursors:
            position = {}
            center = {}
            center['lat'] = cursor['latitude']
            center['lnd'] = cursor['longitude']
            position['center'] = center
            position['type'] = cursor['type']
            positions.append(position)
        return positions


class CarsViewSet(viewsets.ViewSet):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.repository = Repository()
        self.repository.connect()

    def list(self, request):
        data = self.repository.get_car_positions()
        return Response(data)


def main(request):
    context = {}
    return render(request, 'demo/index.html', context)


