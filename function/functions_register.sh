#!/usr/bin/env bash

kubeless function delete ford-function
kubeless function deploy ford-function --runtime python3.6 --from-file function.py --handler function.ford --dependencies requirements.txt
kubeless trigger http create ford --function-name ford-function --hostname f1.2you.io --path ford
sleep 2m
kubectl logs -l function=ford-function
echo "Looks like done"
echo

kubeless function delete toyota-function
kubeless function deploy toyota-function --runtime python3.6 --from-file function.py --handler function.toyota --dependencies requirements.txt
kubeless trigger http create toyota --function-name toyota-function --hostname f1.2you.io --path toyota
sleep 2m
kubectl logs -l function=toyota-function
echo "Looks like done"
echo

kubeless function delete mazda-function
kubeless function deploy mazda-function --runtime python3.6 --from-file function.py --handler function.mazda --dependencies requirements.txt
kubeless trigger http create mazda --function-name mazda-function --hostname f1.2you.io --path mazda
sleep 2m
kubectl logs -l function=mazda-function
echo "Looks like done"
echo
