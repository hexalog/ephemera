import json as jsonlib
from base64 import b64encode
from mqtt import MQTTClient
from function import ford
from function import toyota
from function import mazda


class FunctionTest:
    def __init__(self):
        self.mqtt = MQTTClient(self)

    def on_connect(self, mqttclient, userdata, flags, rc):
        print("Connected with result code " + str(rc))
        self.mqtt.subscribe('faas_locations')
        self.mqtt.subscribe('faas_future_locations')

    def on_message(self, mqttclient, userdata, msg):
        print(msg.payload)
        result = msg.payload.decode("utf-8")
        body = {}
        body['payload'] = b64encode(bytes(result, 'utf-8')).decode('utf-8')
        payloadObj = jsonlib.loads(result)
        body['topic'] = payloadObj['topic']
        event = {}
        event['data'] = body
        ford(event, None)
        toyota(event, None)
        mazda(event, None)

    def start(self):
        self.mqtt.loop_forever()

    def stop(self):
        self.mqtt.disconnect()
        self.mqtt.loop_stop()


if __name__ == "__main__":
    subscriber = None
    try:
        subscriber = FunctionTest()
        subscriber.start()
    except KeyboardInterrupt:
        print("Exiting...")
        if (subscriber is not None):
            subscriber.stop()
