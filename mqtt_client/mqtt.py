import paho.mqtt.client as mqtt
import os
import datetime
import time
import uuid
import json as jsonlib

def make_event(mark,topic,id):

    event = {"mark": mark,
            "car_id": str(uuid.uuid4()), 
            "latitude": id,
            "longitude": 0,
            "timestamp": time.time(),
            "now_iso": datetime.datetime.now().isoformat(),
            "vertical_offset": 0,
            "horizontal_offset": 0,
            "topic": topic}
    return jsonlib.dumps(event)

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
             
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    #client.subscribe("$SYS/#")

    i=0
    while i < 10:
    	ev = make_event("ford","faas_locations",i)
	try:
        	client.publish("faas_locations", ev, True)
    	except Exception as e:
        	print("Failed to publish event:" + str(e))
	i+=1
# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    print(msg.topic+" "+str(msg.payload))

def main():

    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message
    
    try:
        gw = os.environ['MQTT_GATEWAY']
    except KeyError:
        print('Cannot find MQTT gateway')
        return 1

    try:
        port = os.environ['MQTT_PORT']
    except KeyError:
        port = 1883

    try:
        user = os.environ['MQTT_USERNAME']
        pw = os.environ['MQTT_PASSWORD']
        auth = True
    except KeyError:
        print('Use anonymous')
        auth = False


    #client.connect("iot.eclipse.org", 1883, 60)
    if auth:
        client.username_pw_set(user,pw)
    client.connect(gw,port,60)

    # Blocking call that processes network traffic, dispatches callbacks and
    # handles reconnecting.
    # Other loop*() functions are available that give a threaded interface and a    # manual interface.
    client.loop_forever()

