AWS_REGION?=	us-east-2

.PHONY: up
up:
	docker-compose up -d

.PHONY: down
down:
	docker-compose down --remove-orphans
	docker volume prune -f

.PHONY: prune-images
prune-images:
	docker images | grep '^<none>' | awk '{ print $$3 }' | xargs docker rmi

# Docker push
define docker_push
.PHONY: docker-push-$$(subst :,/,$1)
docker-push-$$(subst :,/,$1):
	docker push $1
endef

# Google Container registry push
define gcr_push
.PHONY: gcr-push-$$(subst :,/,$1)
gcr-push-$$(subst :,/,$1):
	gcloud docker -- push $1
endef

# Build docker container
define docker_build =
$1_name=	$$(subst /,,$$(dir $1))
ifeq ($$($1_name),.)
$1_name=	$$(notdir $1)
$1_tags=
$1_tag=		$$($1_name)
else
$1_tag=		$$($1_name):$$(notdir $1)
$1_tags=	$(DOCKER_REPO)/$$($1_tag)
ifdef ECR_REPO
$1_ecr_tags=	$$(ECR_REPO)/$$($1_name):$$(notdir $1)
endif
ifdef GCR_REPO
$1_gcr_tags=	$(GCR_REPO)/$$($1_tag)
endif
endif
$1_tags+=	$(DOCKER_REPO)/$$($1_name):latest
ifdef ECR_REPO
$1_ecr_tags+=	$$(ECR_REPO)/$$($1_name):latest
endif
ifdef GCR_REPO
$1_gcr_tags+=	$(GCR_REPO)/$$($1_name):latest
endif

.PHONY: $1
$1:
	docker build $$(addprefix -t ,$$($1_tags)) $$(addprefix -t, $$($1_ecr_tags)) $$(addprefix -t ,$$($1_gcr_tags)) docker/$1

# Docker push
docker-push-$1: $$(addprefix docker-push-,$$(subst :,/,$$($1_tags)))

$$(foreach tag,$$($1_tags),$$(eval $$(call docker_push,$$(tag))))

# Google Container Registry push
.PHONY: gcr-push-$1
gcr-push-$1: $$(addprefix gcr-push-,$$(subst :,/,$$($1_gcr_tags)))

$$(foreach tag,$$($1_gcr_tags),$$(eval $$(call gcr_push,$$(tag))))

# Amazon ECR push
.PHONY: ecr-repo-$1
ecr-repo-$1:
	$$$$(aws --region $(AWS_REGION) ecr get-login --no-include-email)
	aws --region $(AWS_REGION) ecr describe-repositories --repository-names $$($1_name) >/dev/null 2>&1 || aws --region $(AWS_REGION) ecr create-repository --repository-name $$($1_name)

.PHONY: ecr-push-$1
ecr-push-$1: ecr-repo-$1 $$(addprefix docker-push-,$$(subst :,/,$$($1_ecr_tags)))

$$(foreach tag,$$($1_ecr_tags),$$(eval $$(call docker_push,$$(tag))))
endef

build-docker-build-images: $(DOCKER_BUILD_IMAGES)

$(foreach image,$(DOCKER_BUILD_IMAGES),$(eval $(call docker_build,$(image))))

build-docker-images: build-docker-build-images $(DOCKER_IMAGES)

$(foreach image,$(DOCKER_IMAGES),$(eval $(call docker_build,$(image))))

.PHONY: docker-push-images
push-docker-images: $(addprefix docker-push-,$(DOCKER_IMAGES))

.PHONY: ecr-push-images
push-ecr-images: $(addprefix ecr-push-,$(DOCKER_IMAGES))

.PHONY: gcr-push-images
push-gcr-images: $(addprefix gcr-push-,$(DOCKER_IMAGES))
