
## OVERVIEW
The project represents MQTT end points (autonomous vehicles) accessing and utilizing edge network services deployed as "serverless" functions. The end points report their coordinates and projected locations to the network using MQTT. The MQTT gateway/broker invokes functions via middleware, different functions run depending on the end point type, demonstrating multi-tenancy. Two ways to connect external services to functions are highlighted - using Webhook and Kafka. Ephemera utilizes Kubernetes-based function engine Kubeless for cluster-level scalability and ease of use. Functions are invoked for each location update, store end point locations in back-end Mongo database, calculate end point density and congestion.  The outcome is reported back to the end points via MQTT or Kafka, causing some end points to select alternative tracks. The progress of end points is visualized using Django Web container over Google Maps. Additionally, function load and other telemetry can be visualized using Prometheus and Grafana

### List of components

**End point simulator**  		ephemera-generator

**Webhook enabled MQTT gateway** 	vernemq

**Kafka enabled MQTT broker**		ephemera-mosca

**Functions**				function containers (ford-fn, toyota-fn, mazda-fn), kubeless-controller-manager

**Kafka**				kafka-controller-manager, kafka,zookeeper

**Webhook**				nginx ingress controller

**Peristence DB**			mongo

**Visualization**			ephemera-web


```
[centos@server-edge3-kubernetes-master-host-8nigrw ~]$ kubectl get pods -n default
NAME                         READY     STATUS    RESTARTS   AGE
ephemera-mosca               1/1       Running   0          4d
ephemera-web                 1/1       Running   0          4d
ford-fn-5bd9b79574-p7bsb     1/1       Running   0          5h
iotg                         1/1       Running   0          8d
mazda-fn-dc877fc94-zcs8c     1/1       Running   0          5h
mongo-58dbdb5f4-vx9w8        1/1       Running   0          69d
toyota-fn-5879854cf8-szmjs   1/1       Running   0          5h
```
```
[centos@server-edge3-kubernetes-master-host-8nigrw ~]$ kubectl get pods -n kubeless
NAME                                          READY     STATUS    RESTARTS   AGE
kafka-0                                       1/1       Running   0          18d
kafka-trigger-controller-5b4c8ccc4-lhx72      1/1       Running   3          18d
kubeless-controller-manager-c6b69df76-xh9wx   1/1       Running   0          18d
zoo-0                                         1/1       Running   0          18d
```
```
[centos@server-edge3-kubernetes-master-host-8nigrw ~]$ kubectl get pods -n ingress-nginx
NAME                                        READY     STATUS    RESTARTS   AGE
default-http-backend-55c6c69b88-fzr4q       1/1       Running   0          18d
nginx-ingress-controller-6658c97f58-678kl   1/1       Running   0          18d
```

### Webhook
```
MQTT endpoint <--> MQTT gateway (Vernemq) -> Nginx ctrl -> Function -> Mongo 
				          <---------------
```

**Containers**

ephemera-generator

iotg

kubeless 

nginx

mongo

ephemera-web

### Kafka
```
MQTT endpoint <--> MQTT broker (Mosca) <--> Kafka -> Kafka ctrl -> Function -> Mongo
				                   <---------------
```

**Containers**

ephemera-generator

mosca

kubeless 

kafka

mongo

ephemera-web


## INSTALL

### Kubeless

##### RBAC cluster install 
```
> export RELEASE=$(curl -s https://api.github.com/repos/kubeless/kubeless/releases/latest | grep tag_name | cut -d '"' -f 4)
> kubectl create ns kubeless
> kubectl create -f https://github.com/kubeless/kubeless/releases/download/$RELEASE/kubeless-$RELEASE.yaml
deployment "kubeless-controller-manager" created
serviceaccount "controller-acct" created
clusterrole "kubeless-controller-deployer" created
clusterrolebinding "kubeless-controller-deployer" created
customresourcedefinition "functions.kubeless.io" created
customresourcedefinition "httptriggers.kubeless.io" created
customresourcedefinition "cronjobtriggers.kubeless.io" created
configmap "kubeless-config" created
```


##### Download kubeless CLI latest release https://github.com/kubeless/kubeless/releases [v1.0.0-alpha7]

```
>export OS=$(uname -s| tr '[:upper:]' '[:lower:]')
>export RELEASE=$(curl -s https://api.github.com/repos/kubeless/kubeless/releases/latest | grep tag_name | cut -d '"' -f 4)
>curl -OL https://github.com/kubeless/kubeless/releases/download/$RELEASE/kubeless_$OS-amd64.zip && \
>  unzip kubeless_$OS-amd64.zip && sudo mv bundles/kubeless_$OS-amd64/kubeless /usr/local/bin/
```


##### Deploy a function
```
> kubeless function deploy f4 --runtime python2.7 --from-file f4.py --handler f4.f4
INFO[0000] Deploying function...                        
INFO[0000] Function f4 submitted for deployment         
INFO[0000] Check the deployment status executing 'kubeless function ls f4' 
```

##### Function status
```
> kubeless function ls f4
NAME    NAMESPACE       HANDLER RUNTIME         DEPENDENCIES    STATUS   
f4      default         f4.f4   python2.7                       1/1 READY
```

##### Call function
```
> kubeless function call f4 --data 'Hello world!'
Hello world!
```

##### Kubernetes pod deployed
```
> kubectl get pods -l function=f4
NAME                  READY     STATUS    RESTARTS   AGE
f4-78d99677b7-gm5x7   1/1       Running   0          1m
```

##### Delete a function
```
> kubeless function delete f4
```
##### NGINX ingress controller deployment (Baremetal)
```
> kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/mandatory.yaml
namespace "ingress-nginx" created
deployment "default-http-backend" created
service "default-http-backend" created
configmap "nginx-configuration" created
configmap "tcp-services" created
configmap "udp-services" created
serviceaccount "nginx-ingress-serviceaccount" created
clusterrole "nginx-ingress-clusterrole" created
role "nginx-ingress-role" created
rolebinding "nginx-ingress-role-nisa-binding" created
clusterrolebinding "nginx-ingress-clusterrole-nisa-binding" created
deployment "nginx-ingress-controller" created
>kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/provider/baremetal/service-nodeport.yaml
service "ingress-nginx" created

>kubectl get service -n=ingress-nginx
NAME                   TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)                      AGE
default-http-backend   ClusterIP   10.104.43.44     <none>        80/TCP                       1h
ingress-nginx          NodePort    10.103.207.104   <none>        80:30667/TCP,443:32511/TCP   11m

> POD_NAMESPACE=ingress-nginx
> POD_NAME=$(kubectl get pods -n $POD_NAMESPACE -l app=ingress-nginx -o jsonpath={.items[0].metadata.name})
> kubectl exec -it $POD_NAME -n $POD_NAMESPACE -- /nginx-ingress-controller --version
-------------------------------------------------------------------------------
NGINX Ingress controller
  Release:    0.15.0
  Build:      git-df61bd7
  Repository: https://github.com/kubernetes/ingress-nginx
-------------------------------------------------------------------------------
```


##### NGINX ingress controller deployment (GKE)
##### https://github.com/kubernetes/ingress-nginx/blob/master/docs/deploy/index.md
```
> kubectl patch deployment -n ingress-nginx nginx-ingress-controller --type='json' \
>   --patch="$(curl https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/publish-service-patch.yaml)"
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   148  100   148    0     0    512      0 --:--:-- --:--:-- --:--:--   512
Error from server (NotFound): namespaces "ingress-nginx" not found
> kubectl patch deployment -n ingress-nginx nginx-ingress-controller --type='json'   --patch="$(curl https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/publish-service-patch.yaml)"^C
> curl https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/namespace.yaml \
>     | kubectl apply -f -
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100    63  100    63    0     0    255      0 --:--:-- --:--:-- --:--:--   256
namespace "ingress-nginx" created
> curl https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/default-backend.yaml \
>     | kubectl apply -f -
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  1211  100  1211    0     0   4468      0 --:--:-- --:--:-- --:--:--  4485
deployment.extensions "default-http-backend" created
service "default-http-backend" created
> curl https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/configmap.yaml \
>     | kubectl apply -f -
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   129  100   129    0     0    467      0 --:--:-- --:--:-- --:--:--   469
configmap "nginx-configuration" created
> curl https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/tcp-services-configmap.yaml \
>     | kubectl apply -f -
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100    89  100    89    0     0    359      0 --:--:-- --:--:-- --:--:--   358
configmap "tcp-services" created
> curl https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/udp-services-configmap.yaml \
>     | kubectl apply -f -
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100    89  100    89    0     0    297      0 --:--:-- --:--:-- --:--:--   298
configmap "udp-services" created
> curl https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/rbac.yaml \
>     | kubectl apply -f -
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  2385  100  2385    0     0   9785      0 --:--:-- --:--:-- --:--:--  9814
serviceaccount "nginx-ingress-serviceaccount" created
clusterrole.rbac.authorization.k8s.io "nginx-ingress-clusterrole" created
role.rbac.authorization.k8s.io "nginx-ingress-role" created
rolebinding.rbac.authorization.k8s.io "nginx-ingress-role-nisa-binding" created
clusterrolebinding.rbac.authorization.k8s.io "nginx-ingress-clusterrole-nisa-binding" created
> curl https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/with-rbac.yaml \
>     | kubectl apply -f -
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  1957  100  1957    0     0   4671      0 --:--:-- --:--:-- --:--:--  4681
deployment.extensions "nginx-ingress-controller" created
> kubectl patch deployment -n ingress-nginx nginx-ingress-controller --type='json' \
>   --patch="$(curl https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/publish-service-patch.yaml)"
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   148  100   148    0     0    539      0 --:--:-- --:--:-- --:--:--   540
deployment.extensions "nginx-ingress-controller" patched
> curl https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/provider/gce-gke/service.yaml \
>     | kubectl apply -f -
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   324  100   324    0     0   1302      0 --:--:-- --:--:-- --:--:--  1301
service "ingress-nginx" created
> curl https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/provider/patch-service-with-rbac.yaml | kubectl apply -f -
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  2020  100  2020    0     0   7924      0 --:--:-- --:--:-- --:--:--  7952
deployment.extensions "nginx-ingress-controller" configured
> kubectl get pods --all-namespaces -l app=ingress-nginx --watch
NAMESPACE       NAME                                        READY     STATUS             RESTARTS   AGE
ingress-nginx   nginx-ingress-controller-57b5888889-4zrhm   0/1       CrashLoopBackOff   2          31s
ingress-nginx   nginx-ingress-controller-c7449cb6f-vwfcd    0/1       Terminating        1          47s
ingress-nginx   nginx-ingress-controller-57b5888889-4zrhm   0/1       Running   3         56s
ingress-nginx   nginx-ingress-controller-c7449cb6f-vwfcd   0/1       Terminating   1         1m
ingress-nginx   nginx-ingress-controller-c7449cb6f-vwfcd   0/1       Terminating   1         1m
ingress-nginx   nginx-ingress-controller-57b5888889-4zrhm   1/1       Running   3         1m
^C> kubectl get pods --all-namespaces -l app=ingress-nginx --watch
NAMESPACE       NAME                                        READY     STATUS    RESTARTS   AGE
ingress-nginx   nginx-ingress-controller-57b5888889-4zrhm   1/1       Running   3          2m
^C> POD_NAMESPACE=ingress-nginx
> POD_NAME=$(kubectl get pods -n $POD_NAMESPACE -l app=ingress-nginx -o jsonpath={.items[0].metadata.name})
kubectl exec -it $POD_NAME -n $POD_NAMESPACE -- /nginx-ingress-controller --version
> kubectl exec -it $POD_NAME -n $POD_NAMESPACE -- /nginx-ingress-controller --version
-------------------------------------------------------------------------------
NGINX Ingress controller
  Release:    0.14.0
  Build:      git-734361d
  Repository: https://github.com/kubernetes/ingress-nginx
-------------------------------------------------------------------------------
```


##### Create HTTP trigger
```
> kubeless trigger http create t3 --function-name f3 --hostname f1.2you.io --path f3
INFO[0000] HTTP trigger t3 created in namespace default successfully! 
```

##### Triggering the HTTP trigger
```
> curl --data '{"Another": "Really"}' --header "Content-Type:application/json" http://f1.2you.io/f3
{"Another": "Really"}
```


##### Create Kafka trigger
```
>kubeless function deploy ford-fn --runtime python3.6 --from-file ephemera/function/function.py --handler function.ford --dependencies ephemera/function/requirements.txt --env PUBLISH_PROTO=kafka
INFO[0000] Deploying function...                        
INFO[0000] Function ford-fn submitted for deployment    
INFO[0000] Check the deployment status executing 'kubeless function ls ford-fn' 
>kubeless topic create fn-ford-faas-locations
Created topic "fn-ford-faas-locations".
>kubeless trigger kafka create ford-locations --function-selector created-by=kubeless,function=ford-fn --trigger-topic fn-ford-faas-locations
INFO[0000] Kafka trigger ford-locations created in namespace default successfully! 
```

PUBLISH_PROTO environment variable controls how function publish density and predictions: via Kafka or MQTT

##### Triggering Kafka trigger
```
kubectl exec -ti kafka-0 -n kubeless -- /opt/bitnami/kafka/bin/kafka-console-producer.sh --broker-list localhost:9092 --topic fn-ford-faas-locations
>Hello
>^D

```

## CONTAINER DEPLOYMENT

### VerneMQ MQTT gateway
```
>kubectl apply -f ephemera/yaml/vernemq.yaml
pod "iotg" created
service "iotg" created
> kubectl get svc iotg
NAME      TYPE       CLUSTER-IP       EXTERNAL-IP   PORT(S)          AGE
iotg      NodePort   10.102.135.170   <none>        1883:32107/TCP   6m
> kubectl get pods iotg
NAME      READY     STATUS    RESTARTS   AGE
iotg      1/1       Running   0          26s
```
Based on https://github.com/erlio/docker-vernemq

The YAML config includes:

- configuring MQTT users and passwords (mazda/mazda, ford/ford, toyota/toyota)

- enabling webhooks plugin

- setting up 'on_publish' webhooks pointing at function endpoints, served by nginx (http://toyota-fn:8080 etc.)

To enable debugging output, add environment var DOCKER_VERNEMQ_LOG__CONSOLE__LEVEL=debug

To manage webhooks dynamically, use vmq-admin utility that configures VerneMQ gateway using kubectl exec or remotely
- enable Webhooks plugin   
# vmq-admin set plugins.vmq_webhooks = on
- show current list of configured Webhooks
# vmq-admin webhooks show
- add new Webhook
# vmq-admin webhooks register hook=on_publish endpoint=<function URL> --base64payload=false
- remove new Webhook
# vmq-admin webhooks deregister hook=on_publish endpoint=<function URL>

### Mosca MQTT broker
```
>kubectl apply -f ephemera/yaml/mosca.yaml
pod "ephemera-mosca" created
service "mosca" created
>kubectl get svc mosca
NAME      TYPE       CLUSTER-IP      EXTERNAL-IP   PORT(S)          AGE
mosca     NodePort   10.101.79.100   <none>        1883:32643/TCP   23s
>kubectl get pods ephemera-mosca
NAME             READY     STATUS    RESTARTS   AGE
ephemera-mosca   1/1       Running   0          1m
```
Environment variable:

KAFKA_CONNECTION_STRING: zookeeper.kubeless:2181,zookeeper.kubeless:2181:zookeeper.kubeless:2181

To enable MQTT debug:

DEBUG: ascoltatori:kafka


### Ephemera Web 
```
kubectl apply -f ephemera/yaml/ephemera-web.yaml 
pod "ephemera-web" created
service "ephemera-web" created
> kubectl get svc ephemera-web
NAME           TYPE           CLUSTER-IP       EXTERNAL-IP      PORT(S)        AGE
ephemera-web   LoadBalancer   10.109.107.214   192.168.120.25   80:30245/TCP   23s
> kubectl get pods ephemera-web
NAME           READY     STATUS    RESTARTS   AGE
ephemera-web   1/1       Running   0          1m
```
Environment variables: 

MONGO_DB_HOST: mongo

Note that Ephemera Web is exposed as an external service

### Mongo
```
>kubectl apply -f ephemera/yaml/mongo.yaml 
persistentvolume "mongo-pv-volume" created
persistentvolumeclaim "mongo-pv-claim" created
deployment "mongo" created
service "mongo" created
> kubectl get svc mongo
NAME      TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)     AGE
mongo     ClusterIP   10.99.216.206   <none>        27017/TCP   7s
> kubectl get deployment mongo
NAME      DESIRED   CURRENT   UP-TO-DATE   AVAILABLE   AGE
mongo     1         1         1            1           1m
```
### Prometheus and Grafana

### Deploying/removing functions
```
> ephemera/function/fn_deploy.sh 
INFO[0000] Deploying function...                        
INFO[0000] Function ford-fn submitted for deployment    
INFO[0000] Check the deployment status executing 'kubeless function ls ford-fn' 
INFO[0000] HTTP trigger ford created in namespace default successfully! 
Created topic "fn-ford-faas-future-locations".
Created topic "fn-ford-faas-locations".
INFO[0000] Kafka trigger ford-locations created in namespace default successfully! 
INFO[0000] Kafka trigger ford-future-locations created in namespace default successfully! 
INFO[0000] Deploying function...                        
INFO[0000] Function toyota-fn submitted for deployment  
INFO[0000] Check the deployment status executing 'kubeless function ls toyota-fn' 
INFO[0000] HTTP trigger toyota created in namespace default successfully! 
Created topic "fn-toyota-faas-future-locations".
Created topic "fn-toyota-faas-locations".
INFO[0000] Kafka trigger toyota-locations created in namespace default successfully! 
INFO[0000] Kafka trigger toyota-future-locations created in namespace default successfully! 
INFO[0000] Deploying function...                        
INFO[0000] Function mazda-fn submitted for deployment   
INFO[0000] Check the deployment status executing 'kubeless function ls mazda-fn' 
INFO[0000] HTTP trigger mazda created in namespace default successfully! 
Created topic "fn-mazda-faas-future-locations".
Created topic "fn-mazda-faas-locations".
INFO[0000] Kafka trigger mazda-locations created in namespace default successfully! 
INFO[0000] Kafka trigger mazda-future-locations created in namespace default successfully! 
```

```
> ephemera/function/fn_delete.sh 
INFO[0000] HTTP trigger ford deleted from namespace default successfully! 
INFO[0000] Kafka trigger ford-locations deleted from namespace default successfully! 
INFO[0000] Kafka trigger ford-future-locations deleted from namespace default successfully! 
Topic fn-ford-faas-locations is marked for deletion.
Note: This will have no impact if delete.topic.enable is not set to true.
Topic fn-ford-faas-future-locations is marked for deletion.
Note: This will have no impact if delete.topic.enable is not set to true.
FATA[0000] Failed to delete HTTP trigger object toyota in namespace default. Error: httptriggers.kubeless.io "toyota" not found 
INFO[0000] Kafka trigger toyota-locations deleted from namespace default successfully! 
INFO[0000] Kafka trigger toyota-future-locations deleted from namespace default successfully! 
Topic fn-toyota-faas-locations is marked for deletion.
Note: This will have no impact if delete.topic.enable is not set to true.
Topic fn-toyota-faas-future-locations is marked for deletion.
Note: This will have no impact if delete.topic.enable is not set to true.
INFO[0000] HTTP trigger mazda deleted from namespace default successfully! 
INFO[0000] Kafka trigger mazda-locations deleted from namespace default successfully! 
INFO[0000] Kafka trigger mazda-future-locations deleted from namespace default successfully! 
Topic fn-mazda-faas-locations is marked for deletion.
Note: This will have no impact if delete.topic.enable is not set to true.
Topic fn-mazda-faas-future-locations is marked for deletion.
Note: This will have no impact if delete.topic.enable is not set to true.

```

## Running and Troubleshooting the Demo 

### Running MQTT generator
```
> kubectl apply -f ephemera/yaml/ephemera-generator.yaml

For Kafka use:
MQTT_HOST=mosca
MQTT_USERNAME=ephemera
MQTT_PASSWORD=ephemera

For Webhook use: 
MQTT_HOST=iotg
MQTT_USERNAME=honda
MQTT_PASSWORD=honda

```
### Accessing Ephemera Web  

Ephemera Web is available as an external service via LoadBalancer
To access the demo, use http://<ephemera-web-ip-addr>/demo/
### Troubleshooting MQTT

Check MQTT predictions from MQTT broker to end points
(run from any container in the cluster)
```
>mosquitto_sub -u ephemera -P ephemera -h mosca -t fn-predictions -d

```
Check MQTT locations updates from end point
```
>mosquitto_sub -u ephemera -P ephemera -h mosca -t fn-locations -d
```

### Troubleshooting Kafka

Check ingress messages from MQTT gateway
```
> kubectl exec -ti kafka-0 -n kubeless -- /opt/bitnami/kafka/bin/kafka-console-consumer.sh --broker-list localhost:9092 --topic fn-ford-faas-locations
```

Check egress predictions from functions
```
> kubectl exec -ti kafka-0 -n kubeless -- /opt/bitnami/kafka/bin/kafka-console-consumer.sh --broker-list localhost:9092 --topic fn-density
```

## BUILD
### Building Ephemera Generator
```
> cd ephemera/generator
> sudo docker build -f Dockerfile -t "aa2youio/ephemera_generator:1.0" .
> sudo docker push "aa2youio/ephemera_generator:1.0"
```
### Building Ephemera Web
```
> cd ephemera/visualization
> sudo docker build -f Dockerfile -t "aa2youio/ephemera_web:1.0" .
> sudo docker push "aa2youio/ephemera_web:1.0"
```
### Building Ephemera Mosca
```
> cd ephemera/mosca
> sudo docker build -f examples/kafka/Dockerfile -t "aa2youio/ephemera_mosca:1.0" .
> sudo docker push "aa2youio/ephemera_mosca:1.0"
```

##### List Kubernetes pods
```
> kubectl get pods -n default
NAME                        READY     STATUS    RESTARTS   AGE
f1-7c95b8d8b4-nzm8w         1/1       Running   0          1d
f2-86b4fb6544-vg5sd         1/1       Running   0          1d
f3-7897b64d67-8qf5n         1/1       Running   0          1h
shell-demo                  1/1       Running   0          22h
vernemq1-5c6ff7cdd7-sdxj7   1/1       Running   0          23h
```

##### Access logs for specific function using label attribute of the K8 pod
```
> kubectl logs -l function=f3
Bottle v0.12.13 server starting up (using CherryPyServer())...
Listening on http://0.0.0.0:8080/
Hit Ctrl-C to quit.

10.12.0.1 - - [17/May/2018:03:38:29 +0000] "GET /healthz HTTP/1.1" 200 2 "" "kube-probe/1.8+" 0/148
{'event-time': '2018-05-17 03:38:52.58990684 +0000 UTC', 'extensions': {'request': <LocalRequest: POST http://35.196.166.70/>}, 'event-type': 'application/x-www-form-urlencoded', 'event-namespace': 'cli.kubeless.io', 'data': 'Say hi to Vladimir!', 'event-id': 'dlpR_YF1PALlyDs'}
10.142.0.6 - - [17/May/2018:03:38:52 +0000] "POST / HTTP/1.1" 200 19 "" "kubeless/v1.8.0+$Format:%h$ (linux/amd64) kubernetes/$Format" 0/11944
10.12.0.1 - - [17/May/2018:03:38:59 +0000] "GET /healthz HTTP/1.1" 200 2 "" "kube-probe/1.8+" 0/124
10.12.0.1 - - [17/May/2018:03:39:29 +0000] "GET /healthz HTTP/1.1" 200 2 "" "kube-probe/1.8+" 0/88
[..]
```
 
##### VerneMQ deployment as Docker
> docker run -e "DOCKER_VERNEMQ_USER_HONDA=honda" -e "DOCKER_VERNEMQ_PLUGINS__VMQ_WEBHOOKS=on" -e "DOCKER_VERNEMQ_VMQ_WEBHOOKS__F1__HOOK=on_subscribe" -e "DOCKER_VERNEMQ_VMQ_WEBHOOKS__F1__ENDPOINT=http://f1.2you.io/f1"  -d erlio/docker-vernemq

### Misc useful commands
##### Access logs for specific pod
```
> kubectl logs vernemq1-5c6ff7cdd7-sdxj7 
Password: 
Reenter password: 
2018-05-16 05:55:09.156 [info] <0.31.0> Application plumtree started on node 'VerneMQ@10.12.1.6'
2018-05-16 05:55:09.174 [info] <0.31.0> Application vmq_plugin started on node 'VerneMQ@10.12.1.6'
2018-05-16 05:55:09.174 [info] <0.31.0> Application ssl_verify_fun started on node 'VerneMQ@10.12.1.6'
[..]
2018-05-16 05:55:10.522 [info] <0.31.0> Application vmq_server started on node 'VerneMQ@10.12.1.6'
2018-05-16 05:58:44.698 [info] <0.31.0> Application vmq_webhooks started on node 'VerneMQ@10.12.1.6'
```

##### Find out IP address for specific pod
```
> kubectl describe pods/vernemq1-5c6ff7cdd7-sdxj7|grep IP
IP:             10.12.1.6 
```

##### Access interactive shell for pod
```
> kubectl exec -ti vernemq1-5c6ff7cdd7-sdxj7   -- bash
root@vernemq1-5c6ff7cdd7-sdxj7:/# ps auxww
USER       PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
root         1  0.0  0.0  20092  2920 ?        Ss   May16   0:00 bash /usr/sbin/start_vernemq
vernemq    151  0.0  0.0  11492   112 ?        S    May16   0:00 /usr/lib/vernemq/erts-8.3.5.3/bin/epmd -daemon
vernemq    159  0.0  0.0  19876  1728 ?        S    May16   0:00 /usr/lib/vernemq/erts-8.3.5.3/bin/run_erl -daemon /tmp/vernemq// /var/log/vernemq exec /usr/sbin/vernemq console
vernemq    160  0.1  1.1 3037832 42124 pts/0   Ssl+ May16   2:39 /usr/lib/vernemq/erts-8.3.5.3/bin/beam.smp -P 256000 -A 64 -K true -W w -zdbbl 32768 -- -root /usr/lib/vernemq -progname vernemq -- -home /var/lib/vernemq -- -boot /usr/lib/vernemq/releases/1.3.1/vernemq -config /var/lib/vernemq/generated.configs/app.2018.05.16.05.55.03.config -setcookie vmq -name VerneMQ@10.12.1.6 -smp enable -vm_args /etc/vernemq/vm.args -pa /usr/lib/vernemq/lib/erlio-patches -- console
```

##### Access environment variables
```
root@vernemq1-5c6ff7cdd7-sdxj7:/# env |grep -i honda
DOCKER_VERNEMQ_USER_HONDA=honda
```

### Google (GKE) install


##### Install the Google Cloud SDK
https://cloud.google.com/sdk/docs/quickstarts

##### Create environment variable for correct distribution
```
export CLOUD_SDK_REPO="cloud-sdk-$(lsb_release -c -s)"
```
##### Add the Cloud SDK distribution URI as a package source
```
echo "deb http://packages.cloud.google.com/apt $CLOUD_SDK_REPO main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
```
##### Import the Google Cloud Platform public key
```
curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
```
##### Update the package list and install the Cloud SDK
```
sudo apt-get update && sudo apt-get install google-cloud-sdk
```

##### Initialize the SDK
```
gcloud init
```

Log in into your Google account with Web browser per instructions
Choose project "kubeless"

##### Check Google SDK status
```
> gcloud auth list
          Credentialed Accounts
ACTIVE             ACCOUNT
*                  <account>
```
##### Install kubectl
```
gcloud components install kubectl
```

##### Retrieve current GKE projects
```
> gcloud container clusters list
NAME   LOCATION    MASTER_VERSION  MASTER_IP      MACHINE_TYPE   NODE_VERSION  NUM_NODES  STATUS
kube1  us-east1-b  1.8.8-gke.0     35.196.166.70  n1-standard-1  1.8.8-gke.0   5          RUNNING
kube2  us-east1-b  1.8.8-gke.0     35.196.81.163  n1-standard-1  1.8.8-gke.0   3          RUNNING
```

##### Retrieve kubectl credentials
```
gcloud container clusters get-credentials kube1 --zone us-east1-b 
Fetching cluster endpoint and auth data.
kubeconfig entry generated for kube1.
```

##### Choose Kubernetes cluster
```
> kubectl config get-contexts
CURRENT   NAME                                   CLUSTER                                AUTHINFO                               NAMESPACE
*         gke_kubeless-202902_us-east1-b_kube1   gke_kubeless-202902_us-east1-b_kube1   gke_kubeless-202902_us-east1-b_kube1   
          gke_kubeless-202902_us-east1-b_kube2   gke_kubeless-202902_us-east1-b_kube2   gke_kubeless-202902_us-east1-b_kube2   
> kubectl config use-context gke_kubeless-202902_us-east1-b_kube1
```

