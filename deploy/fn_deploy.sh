#!/usr/bin/env bash
FNDIR=/home/centos/ephemera-fn

MQTT_HOST=mosca
MQTT_USERNAME=ephemera
MQTT_PASSWORD=ephemera
MQTT_PREDICTIONS_TOPIC=fn-predictions
MQTT_DENSITY_TOPIC=fn-density


for fn in ford toyota mazda; do 
	kubeless function ls ${fn}-fn > /dev/null 2>&1
	if [ $? -eq 0 ] ; then
        	kubeless function delete ${fn}-fn
	fi
        kubeless function deploy ${fn}-fn --runtime python3.6 --from-file ${FNDIR}/function.py --handler function.${fn} --dependencies ${FNDIR}/requirements.txt --env PUBLISH_PROTO=kafka
        kubeless trigger http create ${fn} --function-name ${fn}-fn --path ${fn}

        kubeless topic create fn-${fn}-faas-future-locations
        kubeless topic create fn-${fn}-faas-locations
        kubeless trigger kafka create ${fn}-locations --function-selector created-by=kubeless,function=${fn}-fn --trigger-topic fn-${fn}-faas-locations
        kubeless trigger kafka create ${fn}-future-locations --function-selector created-by=kubeless,function=${fn}-fn --trigger-topic fn-${fn}-faas-future-locations
done
