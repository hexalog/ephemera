import os
import datetime
import uuid
import threading
import json as jsonlib
import paho.mqtt.client
from time import sleep
from distance import calc_distance
from distance import calc_distance2
from mqtt import MQTTClient
from gpstrack import Point

MQTT_PREDICTIONS_TOPIC = 'fn-predictions'
MQTT_DENSITY_TOPIC = 'fn-density'
POS_UPDATE_FREQ = 1


class Car:
    def __init__(self, track1, track2, speed, offset, type, pw, active_mode):
        '''
        Car constructor
        :param track: GPS track
        :param speed: Car speed in kilometers per hour
        :param offset: Initial offset - number of GPS point
        :param type: Car make
        '''
        self.track = track1
        self.track1 = track1
        self.track2 = track2
        self.position = offset
        self.speed = speed
        self.original_speed = speed
        self.offset = offset
        self.pw = pw
        self.type = type
        self.total_distance = 0.0
        self.current_time = datetime.datetime.now()
        self.total_time = 0
        self.distance = 0
        self.car_id = str(uuid.uuid4())
        self.center_longitude = "0.0"
        self.center_latitude = "0.0"
        self.center = Point(self.center_longitude, self.center_latitude)
        self.vertical_offset = 0
        self.horizontal_offset = 0
        self.lock = threading.Lock()
        self.future_location_event = None
        self.location_event = None
        self.change_speed = True
        self.active_mode = active_mode
        try:
            change_speed = os.environ['CHANGE_SPEED']
            if change_speed == "no":
                self.change_speed = False
        except KeyError:
            pass
        self.changed = None

        try:
            self.predictions_topic = os.environ['MQTT_PREDICTIONS_TOPIC']
        except KeyError:
            self.predictions_topic = MQTT_PREDICTIONS_TOPIC
        try:
            self.density_topic = os.environ['MQTT_DENSITY_TOPIC']
        except KeyError:
            self.density_topic = MQTT_DENSITY_TOPIC

        self.mqtt = MQTTClient(self)
        if self.active_mode == True:
            self.mqtt.loop_start()

    def on_connect(self, mqttclient, userdata, flags, rc):
        print("MQTT: connected with result code " + str(rc))
        (res, mid) = mqttclient.subscribe([(self.predictions_topic, 0), (self.density_topic, 0)])
        if (res != paho.mqtt.client.MQTT_ERR_SUCCESS):
            print("Failed to subscribe to MQTT topics")

    def on_disconnect(self, mqttclient, userdata, rc):
        print("MQTT: disconnected with result code " + str(rc))
        if rc != 0:
            try:
                self.mqtt.reconnect()
            except Exception as e:
                print("MQTT reconnect failed: %s" % str(e))

    def on_message(self, mqttclient, userdata, msg):
        result = msg.payload.decode("utf-8")
        json = jsonlib.loads(result)

        if (json['car_id'] == self.car_id):
            print("Car: {} {} on_message: {} {}".format(self.car_id, self.type, msg.topic, result))
            with self.lock:
                if msg.topic == self.predictions_topic:
                    prediction = json['prediction']
                    if prediction > 10:
                        self.track = self.track2
                        self.change_speed = False
                if msg.topic == self.density_topic:
                    density = int(json["density"])
                    if density == 1 and self.speed != self.original_speed:
                        print("Car: {}, type: {}, density: {}, change speed {} -> {}".format(self.car_id, self.type,
                                                                                             density, self.speed,
                                                                                             self.original_speed))
                        self.speed = self.original_speed

                    if not self.change_speed:
                        return
                    if density > 1:
                        new_speed = self.original_speed / density
                        if int(1000000 * self.speed) != int(1000000 * new_speed):
                            if density > 15:
                                print("Car: {}, type: {}, density: {}, change speed {} -> {}".format(self.car_id, self.type,
                                                                                                     density, self.speed,
                                                                                                     new_speed))
                                self.speed = new_speed

    def start(self):
        try:
            while True:
                res = self.do_step()
                if not res:
                    return
        except KeyboardInterrupt:
            print("Exiting...")

    def publish_events(self):
        if not self.future_location_event is None:
            self.mqtt.publish_future_location(self.future_location_event)
        if not self.location_event is None:
            self.mqtt.publish_location(self.location_event)
            self.location_event = None

    def do_step(self):
        sleep(POS_UPDATE_FREQ)
        with self.lock:
            if self.position >= len(self.track.track):
                print(
                    "Car: {} type: {}. end of track, position = {}, total distance = {:.2f} km, total time = {:.2f} minutes".format(
                        self.car_id, self.type, self.position, self.total_distance / 1000.0, self.total_time / 60.0))
                if self.active_mode == True:
                    self.mqtt.loop_stop()
                    self.mqtt.disconnect()
                del self.mqtt
                return False

            if self.position < len(self.track.track) - 1:
                if (self.distance == 0):
                    self.distance = calc_distance(
                        self.track.track[self.position], self.track.track[self.position + 1])
                    self.total_distance += self.distance

            if self.changed is None or self.changed == True:
                position = self.track.track[self.position]
                self.vertical_offset = round(
                    calc_distance2(self.center.longitude, self.center.latitude, position.longitude,
                                   self.center.latitude) / 50)
                self.horizontal_offset = round(
                    calc_distance2(self.center.longitude, self.center.latitude, self.center.longitude,
                                   position.latitude) / 50)
                self.location_event = self.make_event(self.mqtt.locations_topic, self.position)
                self.changed = False

            now = datetime.datetime.now()
            diff = (now - self.current_time).total_seconds()
            speed_in_meters_per_second = (1000 * self.speed) / 3600
            # print("Car: {} calculate dist={} speed={} diff={} total={}".format(self.type, self.distance, self.speed, diff, self.total_distance))
            if self.distance <= speed_in_meters_per_second * diff:
                next_position = self.calc_next_point(80)
                if (next_position < len(self.track.track) - 1):
                    self.future_location_event = self.make_event(self.mqtt.future_locations_topic, next_position)
                self.position += 1
                self.distance = 0
                self.current_time = now
                self.total_time += diff
                self.changed = True
                # print("Car: {} step - next pos={}".format(self.type, self.position))
        self.publish_events()
        return True

    def calc_next_point(self, distance):
        next_position = self.position + 1
        diff = 0
        while next_position < len(self.track.track) - 1 and diff < distance:
            diff += calc_distance(self.track.track[self.position],
                                  self.track.track[next_position])
            next_position += 1
        return next_position

    def make_event(self, event_type, position):
        point = self.track.track[position]
        vertical_offset = round(calc_distance2(self.center.longitude, self.center.latitude, point.longitude,
                                               self.center.latitude) / 50)
        horizontal_offset = round(
            calc_distance2(self.center.longitude, self.center.latitude, self.center.longitude,
                           point.latitude) / 50)
        print(
            "Car: {} type: {} distance: {} meters, event type: {} square: {},{}".format(self.car_id, self.type,
                                                                                  int(self.distance), event_type,
                                                                                  vertical_offset, horizontal_offset))
        event = {"type": self.type,
                 "car_id": self.car_id,
                 "latitude": point.latitude,
                 "longitude": point.longitude,
                 "timestamp": datetime.datetime.now().timestamp(),
                 "now_iso": datetime.datetime.now().isoformat(),
                 "vertical_offset": vertical_offset,
                 "horizontal_offset": horizontal_offset,
                 "topic": event_type}
        return jsonlib.dumps(event)
