import argparse
import os
import xml.etree.ElementTree as et
from distance import calc_dictance
from gpstrack import Point


def read_file(file):
    # validate
    if not os.path.exists(file):
        print("File {} doesn't exists".format(file))
        return None
    if not os.path.isfile(file):
        print("{} is not file".format(file))
        return None
    # read file as string
    with open(file, 'r') as f:
        return f.read()


def write_file(file, lines):
    with open(file, 'w') as f:
        f.write("\n".join(lines))
        f.write("\n")


def prepare(google, output):
    xml = read_file(google)
    root = et.fromstring(xml)
    kml_ns = {'kml': 'http://www.opengis.net/kml/2.2'}
    line = root.find('.//kml:LineString', kml_ns)
    coordinates = line.find('.//kml:coordinates', kml_ns)
    text = coordinates.text
    points = text.splitlines()
    # print(points)
    points[:] = [point.strip()[:-2]
                 for point in points if not len(point.strip()) == 0]
    # print(points)
    # print(len(points))
    new_points = []
    for i in range(len(points) - 1):
        curr_point = points[i]
        next_point = points[i + 1]
        curr_longitude, curr_latitude = curr_point.split(',')
        next_longitude, next_latitude = next_point.split(',')
        distance = int(calc_dictance(
            Point(curr_longitude, curr_latitude), Point(next_longitude, next_latitude)))
        # print(distance)
        if distance < 50:
            new_points.append(curr_point)
            continue
        curr_longitude = float(curr_longitude)
        curr_latitude = float(curr_latitude)
        next_longitude = float(next_longitude)
        next_latitude = float(next_latitude)
        count = int(distance / 50) + 1
        diff_longitude = (next_longitude - curr_longitude) / count
        diff_latitude = (next_latitude - curr_latitude) / count
        for j in range(count):
            new_longitude = curr_longitude + (diff_longitude * (j + 1))
            new_latitude = curr_latitude + (diff_latitude * (j + 1))
            new_point = "{:.5f},{:.5f}".format(new_longitude, new_latitude)
            new_points.append(new_point)
    write_file(output, new_points)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Transform and enhance google gps track.')
    required = parser.add_argument_group('required arguments')
    required.add_argument('--google', required=True,
                          help='Gps track file in google format')
    required.add_argument('--output', required=True,
                          help='Transformed and enhanced track file')
    args = parser.parse_args()
    prepare(args.google, args.output)
