## Pre requirements
```bash
# install python modules
pip3 install pymongo
pip3 install paho-mqtt
pip3 install geopy

# install mongodb
sudo apt install mongodb
sudo mkdir -p /data/db/
sudo chown `id -u` /data/db

# install mqtt
sudo apt install mosquitto mosquitto-clients
```

## Running
```bash
# run mongodb server
mongod --smallfiles
# --smalfiles is optional for local testing

# subscribe to event collection from command line
mosquitto_sub -h localhost -t faas_locations
mosquitto_sub -h localhost -t faas_predictions
mosquitto_sub -h localhost -t faas_density

# start function test subscriber
python3 function_test.py

# run generator
python3 generator.py
or
MQTT_HOST='10.12.1.6'; MQTT_PORT='1883'; python3 generator.py
or
MONGODB_HOST=mongo; CAR_SPEED=40; python3 generator.py

```

## Generator running in Docker
```bash
./docker_build.sh
./docker_run.sh
```

## Functions registration
```bash
./functions_register.sh
```
