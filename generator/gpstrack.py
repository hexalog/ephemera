# GPS location point
class Point:
    def __init__(self, longitude, latitude):
        self.longitude = float(longitude)
        self.latitude = float(latitude)

    def __str__(self):
        return 'GPS point - longtitude: {}, latitude: {}'.format(self.longitude, self.latitude)


# GPS track contained a number of GPS location points
class Track:
    def __init__(self, file):
        self.track = []
        with open(file, "r") as f:
            lines = f.readlines()
            for i, line in enumerate(lines):
                longitude, latitude = line.split(',')
                self.track.append(Point(longitude, latitude))

    def __str__(self):
        return ''.join(str(point) for point in self.track)
