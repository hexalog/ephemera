import os
from time import sleep
from paho.mqtt.client import Client as MqttClient
import paho.mqtt.publish as publish
import traceback
import json as jsonlib

#MQTT_HOST = "localhost"
MQTT_HOST="mosca"
MQTT_PORT=1883
MQTT_SUB_HOST="iotg"
MQTT_SUB_PORT=1883

MQTT_LOCATIONS_TOPIC="faas-locations"
MQTT_FUTURE_LOCATIONS_TOPIC="faas-future-locations"

PUBLISH_MAX_ATTEMPTS = 10


class MQTTClient:
    def __init__(self, subscriber):

        try:
            self.host = os.environ['MQTT_HOST']
        except KeyError:
            self.host = MQTT_HOST
        try:
            self.port = int(os.environ['MQTT_PORT'])
        except KeyError:
            self.port = MQTT_PORT
        try:
            self.user = os.environ['MQTT_USER']
        except KeyError:
            self.user = subscriber.type
        try:
            self.pw = os.environ['MQTT_PW']
        except KeyError:
            self.pw = subscriber.type
        #self.auth = {'username': self.user, 'password': self.pw}
        self.client_id = subscriber.car_id

        print("MQTT Init: Car #%s [%s/%s] @ %s:%s" %
              (self.client_id, self.user, self.pw, self.host, self.port))

        self.mqtt = MqttClient(subscriber.car_id)
        self.mqtt.username_pw_set(self.user, self.pw)
        self.mqtt.on_connect = subscriber.on_connect
        if subscriber.active_mode == True:
            self.mqtt.on_message = subscriber.on_message
            self.mqtt.on_disconnect = subscriber.on_disconnect
        self.mqtt.connect(self.host, self.port, 60)
        print ("MQTT Connect: ID %s" % subscriber.car_id)

        self.locations_topic = MQTT_LOCATIONS_TOPIC
        self.future_locations_topic = MQTT_FUTURE_LOCATIONS_TOPIC

    def publish_future_location(self, event):
        # ten attempts
        count = PUBLISH_MAX_ATTEMPTS
        while count != 0:
            try:
                # publish.single(topic=self.future_locations_topic, payload=event,
                #               hostname=self.host, port=self.port, client_id=self.client_id, auth=self.auth, retain=True)
                (ret,mid) = self.mqtt.publish(self.future_locations_topic, event, True)
                print("Car #%s published future location msg ID #%s: %s" % (self.client_id, mid, jsonlib.dumps(event)))
                return
            except Exception as e:
                print("Failed to publish future location:" + str(e))
                count -= 1
                sleep(0.05)

    def publish_location(self, event):
        # ten attempts
        count = PUBLISH_MAX_ATTEMPTS
        while count != 0:
            try:
                # publish.single(topic=self.locations_topic, payload=event,
                #               hostname=self.host, port=self.port, client_id=self.client_id, auth=self.auth, retain=True)
                (ret,mid)=self.mqtt.publish(self.locations_topic, event, True)
                print("Car #%s published current location msg ID #%s: %s" % (self.client_id, mid, jsonlib.dumps(event)))
                return
            except Exception as e:
                print("Failed to publish location:" + str(e))
                count -= 1
                sleep(0.05)

    def loop_start(self):
        self.mqtt.loop_start()

    def loop_stop(self):
        self.mqtt.loop_stop()

    def subscribe(self, topic):
        self.mqtt.subscribe(topic)

    def loop_forever(self):
        self.mqtt.loop_forever()

    def disconnect(self):
        self.mqtt.disconnect()

    def reconnect(self):
        self.mqtt.reconnect()
