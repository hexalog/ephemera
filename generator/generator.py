import os
import sys
import threading
import datetime
import traceback
from time import sleep
from pymongo import MongoClient
from gpstrack import Track
from car import Car

CAR_COUNT = 1
CAR_SPEED = 150

CAR1_TYPE = "ford"
CAR2_TYPE = "toyota"
CAR3_TYPE = "mazda"
CAR1_PW = "ford"
CAR2_PW = "toyota"
CAR3_PW = "mazda"

#MONGODB_HOST = "localhost"
MONGODB_HOST = "mongo"
MONGODB_PORT = 27017
MONGODB_DATABASE = "faas_demo"
MONGODB_COLLECTION = "car_events"


def run_car(delta1, offset, speed, type, pw, active_mode):
    speed += delta1 * 3
    car = Car(track1, track2, speed, offset, type, pw, active_mode)
    try:
        car.start()
    except Exception as e:
        print ("exception car #%s %s" % (car.car_id, str(e)))
        traceback.print_exc()


if __name__ == "__main__":

    car_count = CAR_COUNT

    try:
        car_count = int(os.environ['CAR_COUNT'])
    except KeyError:
        pass
    except Exception as e:
        print(e)
        print("Exiting...")
        exit(1)

    car_speed = CAR_SPEED
    try:
        car_speed = int(os.environ['CAR_SPEED'])
    except KeyError:
        pass
    except Exception as e:
        print(e)
        print("Exiting...")
        exit(1)

    car1_type = CAR1_TYPE
    try:
        car1_type = os.environ['CAR1_TYPE']
    except KeyError:
        pass
    car2_type = CAR2_TYPE
    try:
        car2_type = os.environ['CAR2_TYPE']
    except KeyError:
        pass
    car3_type = CAR3_TYPE
    try:
        car3_type = os.environ['CAR3_TYPE']
    except KeyError:
        pass

    car1_pw = CAR1_PW
    try:
        car1_pw = os.environ['CAR1_PW']
    except KeyError:
        pass
    car2_pw = CAR2_PW
    try:
        car2_pw = os.environ['CAR2_PW']
    except KeyError:
        pass
    car3_pw = CAR3_PW
    try:
        car3_pw = os.environ['CAR3_PW']
    except KeyError:
        pass

    track1 = Track('track_3.track')
    track2 = Track('track_4.track')

    threads = []

    try:
        mqtt_host = os.environ['MONGODB_HOST']
    except KeyError:
        mongodb_host = MONGODB_HOST
    MONGODB_CONNECTION_URL = "mongodb://" + MONGODB_HOST + \
        ":" + str(MONGODB_PORT) + "/" + MONGODB_DATABASE

    try:
        mongoclient = MongoClient(MONGODB_CONNECTION_URL, maxPoolSize=500)
        db = eval('mongoclient.' + MONGODB_DATABASE)
        collection = eval('db.' + MONGODB_COLLECTION)
        collection.remove()
        for delta in range(0, car_count):
            thread = threading.Thread(target=run_car, args=(
                delta, 140 + 0, car_speed, car1_type, car1_pw, True))
            threads.append(thread)
            thread.start()

        for delta in range(0, car_count):
            thread = threading.Thread(target=run_car, args=(
                delta, 140 + 10, car_speed, car2_type, car2_pw, True))
            threads.append(thread)
            thread.start()
        for delta in range(0, car_count):
            thread = threading.Thread(target=run_car, args=(
                delta, 140 + 20, car_speed, car3_type, car3_pw, True))
            threads.append(thread)
            thread.start()
        for delta in range(0, 40):
            thread = threading.Thread(target=run_car, args=(
                0, 214, 0.00001, car3_type, car3_pw, False))
            thread.start()
        for thread in threads:
            thread.join()
            print("thread joined")
        collection.remove()
        mongoclient.close()
        print()
        print("Current time: {}".format(datetime.datetime.now().isoformat()))
        sys.stdout.flush()
        sleep(60)
        os.kill(os.getpid(), 9)
    except KeyboardInterrupt:
        print()
        print("Exiting...")
        os.kill(os.getpid(), 9)
    except:
        print('mongo connection failed {}'.format(sys.exc_info()[0]))
        exit(1)
