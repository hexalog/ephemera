import geopy.distance


def calc_distance(point1, point2):
    coords_1 = (point1.latitude, point1.longitude)
    coords_2 = (point2.latitude, point2.longitude)
    return geopy.distance.vincenty(coords_1, coords_2).m


def calc_distance2(left_latitude, left_longitude, right_latitude, right_longitude):
    coords_1 = (left_latitude, left_longitude)
    coords_2 = (right_latitude, right_longitude)
    return geopy.distance.vincenty(coords_1, coords_2).m
